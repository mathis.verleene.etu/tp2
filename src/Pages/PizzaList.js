import Component from '../components/Component.js';
import Img from '../components/Img.js';
import PizzaThumbnail from '../components/PizzaThumbnail.js';
export default class PizzaList extends Component {
	#pizzas;
	set pizzas(value) {
		this.#pizzas = value;
		this.children = this.#pizzas.map(pizza => new PizzaThumbnail(pizza));
	}

	constructor(pizzas) {
		super('section', { name: 'class', value: 'pizzaList' });
		this.pizzas = pizzas;
	}
}
