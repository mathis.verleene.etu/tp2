import Component from './components/Component.js';
export default class Router {
    static titleElement;
    static contentElement;
    static routes;

    static navigate(path) {
        this.routes.forEach(e => {
            if (e.path == path) {
                const title = new Component('h1', null, e.title);
                Router.titleElement.innerHTML = title.render();
                Router.contentElement.innerHTML = e.page.render();
            }
        });
    }
}