class Component {
    tagName;
    children;
    constructor(tagName, attribute, children) {
        this.tagName = tagName;
        this.attribute = attribute;
        this.children = children;
    }
    render() {
        if (
            typeof this.attribute !== 'undefined' &&
            this.attribute != null &&
            typeof this.children !== 'undefined' &&
            this.children != null
        ) {
            if (Array.isArray(this.children)) {
                let string_res = `<${this.tagName} ${this.attribute.name}="${this.attribute.value}" >`;
                this.children.forEach(e => {
                    if (e instanceof Component) {
                        string_res += e.render();
                    } else {
                        string_res += `${e}`;
                    }
                });
                string_res += `</${this.tagName}>`;
                return string_res;
            } else {
                return `<${this.tagName} ${this.attribute.name}="${this.attribute.value}" >${this.children}</${this.tagName}`;
            }
        }
        if (typeof this.children !== 'undefined' && this.children != null) {
            if (Array.isArray(this.children)) {
                let string_res = `<${this.tagName}>`;
                this.children.forEach(e => {
                    if (e instanceof Component) {
                        string_res += e.render();
                    } else {
                        string_res += `${e}`;
                    }
                });
                string_res += `</${this.tagName}>`;
                return string_res;
            } else {
                return `<${this.tagName}>${this.children}</${this.tagName}>`;
            }
        }
        if (typeof this.attribute !== 'undefined' && this.attribute != null) {
            return `<${this.tagName} ${this.attribute.name}="${this.attribute.value}" />`;
        }
        return `<${this.tagName} />`;
    }
}
export default Component;