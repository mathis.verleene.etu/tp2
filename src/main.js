import Component from './components/Component.js';
import data from './data.js';
import Img from './components/Img.js';
import PizzaThumbnail from './components/PizzaThumbnail.js';
import PizzaList from './Pages/PizzaList.js';
import Router from './Router.js';
// const title = new Component('h1', null, ['La', ' ', 'carte']);
// document.querySelector('.pageTitle').innerHTML = title.render();

// document.querySelector('.pageContent').innerHTML = pizzaList.render();

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList([]);
Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data;
Router.navigate('/'); // affiche la liste des pizzas